<!doctype html>
<html lang="en">
<head>
	<!-- This page is generated from templates -->
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="alternate" type="application/rss+xml" href="http://journal.imaginary-realities.com/feed_rss2.xml" title="Imaginary Realities release feed"/>
	<link href="../../../css/reset.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style-desktop.css" rel="stylesheet" media="screen and (min-device-width:768px) and (min-width:768px), print, projection"/>

	<link href="../../../css/style-article.css" rel="stylesheet" media="screen, handheld, print, projection"/>

	<script src="../../../js/modernizr.js"></script>
  	
	<title>Imaginary Realities - 
	The Hunger Game, or how I learned to break the ship from the bottle
</title>
</head>
<body>
    <header>
	<div class="header">
		<div class="header-name"><a href="../../../index.html">IMAGINARY REALITIES</a></div>
		<div class="header-name"><a href="../index.html">Volume 5, Issue 1</a></div>
	</div>
	<div class="headermenu">

		<span class="headermenu-link"><a href="../index.html">Table of Contents</a></span><span class="headermenu-link"><a href="../../../contribute.html">Contribute An Article</a></span>

	</div>
	</header>
	<main>
	<div class="main">

	<div class="mainsection">
		<div class="mainheader"><a name="article-title">
	The Hunger Game, or how I learned to break the ship from the bottle
</a></div>
		<div class="mainbody">

<p class="byline">
by Michael <span class="author-nick">Drakkos</span> Heron, 19th August 2013
</p>


  <p>
    Before you start developing a MUD, you need to realize one key thing — it's going to take a long time to make it something people want to play. You can technically tick the “have my own MUD” box in a matter of minutes if you like, but to operate a MUD that’s worth people’s time, you need original content, original game systems, and interesting areas that properly reflect the game theme you’ve chosen. You need enough of each of these to hold someone's interest long enough for them to want to come back.
  </p>
  <p>
    You also need to expect that all the work that goes into making this game will fall on <em>your</em> shoulders. You can post around asking for builders and coders, but unless you make a spectacularly good pitch, you're unlikely to have much joy; it's very much a seller's market. There are dozens of projects started every month, and someone who knows how to make things happen is never going to lack for offers. If all you are bringing to the table is “management” or “a server” or “an idea” — well, I wish you the best of luck, but that’s a very tough sell.
  </p>
  <p>
    Finally, you need to work to the expectation that even if you do beat the odds and end up with a MUD that opens properly, you're going to be one of the 95% of MUDs that get no significant player numbers. It's not that your MUD is one of the “bad ones”, it's that we work in a niche field: text based multiplayer gaming via what are, in many ways, obsolete protocols. We're a brand that has almost no market recognition. There's a very small pool of people looking for a MUD, and there are already twenty or so well-established MUDs that can each boast extensive content and a vibrant social community with at least a hundred players online at all times.
  </p>
  <p>
    What I'm saying is: choosing to start a MUD is choosing to participate in all-but-inevitable failure. Yet, back in 2009, that's exactly what I chose to do.
  </p>
  <p>
    It's not that I didn't know better. I'd been mudding since my first year of university, back in 1996. I'd been a developer on <span class="gametitle">Discworld MUD</span> since 1999. From 2005 to 2008 I was working on my own game engine — a Java MMO builder called <span class="softwaretitle">Swordsmith</span>. I remember the high points of the ‘90s when player counts only ever trended up, and I remember the slump that followed the first graphical MMOs, when player numbers only ever trended downward. I've never been much for integrating into the greater MUD community, but I read all the boards, I knew all the conventional wisdom. I knew the finality and pessimism that was the day-to-day atmosphere of our hobby.
  </p>
  <p>
    I had every reason not to do it, in other words.
  </p>
  <p>
    It's not even that I was stuck because I had no other options — I'm a pretty good coder, and I know dozens of languages. I had all kinds of ways to scratch a creative itch, and none of them had to be a MUD, but that's what I wanted to do. It's what I love doing, and even in my academic career as a researcher into accessibility and games, I've looked for opportunities to discuss text games where I can — see my previous works, <a href="http://www.scribd.com/doc/134002484/M-Heron-Likely-to-be-eaten-by-a-Grue-the-relevance-of-text-games-in-the-modern-era">“‘Likely to be eaten by a Grue’ — the relevance of text games in the modern era”</a> and <a href="http://tinyurl.com/mk6v3k2">“Authorship and Auteurship in the Collaborative Development Process of Text-Based Games”</a>, the latter with John Townsend.
  </p>
  <p>
    I believe there is an audience out there for MUDs — we don't have to work under the assumption that what we're doing is building our own little ship in a bottle. I started with the intention to make a great MUD that people played. In this article, I want to talk about how that worked and give a “lessons learned” summary on taking a MUD from bare bones to fully-released game.
  </p>
  <h3>Oh, the infrastructure you'll need</h3>
  <p>
    Let's assume you've read all the above and decided, “Screw it, I want a MUD, so I'm going to make a MUD, and if you don't like it you can ride the suck-it train all the way to its terminus.” That's good — you have to <em>want</em> to do this, and if you don't have a burning in you to make it happen, you're not going to get anywhere. It's a long, long slog to get to the end and it's going to take years of your life. If you can be put off by my saying your failure is inevitable, then, well, your failure <em>was</em> inevitable. The next step is deciding where you start. Generally, you need two things, although strictly speaking both are optional.
  </p>
  <p>
    The first is someplace dedicated to run your game. <a class="gametitle" href="http://epitaphonline.co.uk/">Epitaph</a>, my MUD, used <a class="website" href="https://www.linode.com/">Linode</a> early on, and has since moved to <a class="website" href="https://www.bhost.net/home?afid=119631">Bhost</a>. We have Bhost’s largest package (two of them, in fact), which is fast, has excellent bandwidth, and is generously provisioned. You can just run your MUD from your own computer, of course, but don't undervalue the benefits of having something external. It makes it easier for people to come try it out, for one thing, and if nothing else it means you can have a chat room for your friends to use while you get on with the business of building the game. Every time someone tries to connect and is denied reduces the likelihood they will try again.
  </p>
  <p>
    Don't underestimate the value of having people there, either, even if only for conversation. It gets lonely on a MUD all by yourself, no matter how much fun you may be having.
  </p>
  <p>
    The second thing you need is a mudlib, or a codebase. That's your starting point. I know some of you are reading this and saying “Oh, it's okay, I'm going to code that from scratch”. You folks — this isn't the article for you. I get that you want to do it, and you'll be having a lot of fun with it (as I did with Swordsmith), but you're not building a game. Seriously — you have years of work ahead of you before you're even ready to <em>start</em> building a game. If you're interested in the technical challenge, that's great — more power to your elbow. If, however, you want to make a <em>game</em>, then that's really not the place to start.
  </p>
  <p>
    What you want to start with is <em>anything</em>. Seriously — do not stress about or overanalyze this. Pick the codebase that you're most familiar with, or the codebase that's most popular for the kind of game you want to make. I'd recommend an LPC codebase, but that's because I'm an LPC guy. Just pick one — the fact that it isn't going to be able to do the things you want it to do is irrelevant. As the years go by, you'll make it do the things you want it to do.
  </p>
  <p>
    We started on <span class="gametitle">Epitaph</span> with a barebones <a class="softwaretitle" href="http://en.wikipedia.org/wiki/Discworld_mudlib">Discworld mudlib</a>. Not the one that's generally available — being an ex-admin there did come with some perks. We were also hosted in the short term by Sojan of <span class="gametitle">Discworld</span>, who generously made server space available. This by itself ensured that “starting a MUD” was instantly something more than “a crazy notion I'll get around to”. My lengthy experience with <span class="gametitle">Discworld</span> and the mudlib itself, which has my dirty fingerprints all over its creaky innards, was the sole reason I picked it. Seriously, just pick anything. Then treat it like a musical instrument that you can retune yourself. You'll make it sing in exactly the ways you want it to.
  </p>
  <h3>The initial development</h3>
  <p>
    The first question is, “where do I start?” It's tempting to just dive in and start coding up areas and items and NPCs. My advice is: don't do that. It was six months of hard work before we had more than a couple of rooms on <span class="gametitle">Epitaph</span>. That was largely because I knew what I wanted to do to pimp out the mudlib. I knew what I wanted to change, how I wanted to change it, and the kind of game experience I was aiming to provide. You might not know that right away, but that's fine. The first few steps of development should be planning.
  </p>
  <p>
    Put up a wiki. Seriously.
  </p>
  <p>
    It doesn't matter if it's only ever you who will see it, make one available so you can start plotting out your ideas and <em>try</em> to get other people to check it out. A wiki is accessible from anywhere that has an Internet connection, and the freeform syntax means it's really easy to structure even complicated ideas. It's also something you can point interested people towards. It's going to be a year, at least, until you have something genuinely worth showing people in terms of code. Your ideas might be exactly what you need to sell people on your vision. Get as many people as you can discussing your ideas.
  </p>
  <p>
    At all times, be mindful of one key question — where is the <em>fun</em>? Remember, games are supposed to be fun, and they're supposed to be engaging. At this stage, you're going to experience a very common pattern: you'll start off narrowly focused on a game theme, then your focus will rapidly expand and go off on tangents and explore new directions, experimental systems, and so on. Then you'll realize, “oh yes, I have to code all of this,” which is when you'll see what kind of developer you are.
  </p>
  <p>
    Developers who are going to succeed will do some kind of prioritization here — they'll underline the things that they absolutely must have, and the things that they really <em>should</em> have. They'll put the things they <em>could</em> have off to the side, and then completely mothball the stuff that they <em>want</em> but don’t <em>need</em>. Those developers will have a firm estimate of development time in mind (it’ll be wrong, but it’ll be firm) and a good sense of what they can reasonably achieve in that time. They will kill off even their most cherished ideas if they're not feasible, or not critical to the game experience they want to make. They will know what's important architecture and what is mostly “designerbation”. They will, in the old editing wisdom, “kill their darlings”.
  </p>
  <p>
    Developers who are going to fail won't do any of that. Every idea will become a promise they have made to themselves. They'll be unable to let anything go, and in the end will have so much plotted and planned out that they've already failed. Don't overwhelm yourself, and don't let yourself (or others) write cheques your ability can't cash. Ideas are cheap and people generally dispense them like tic-tacs. That's awesome as part of a scope-setting or brainstorming effort, but it's terrible as part of a development agenda.
  </p>
  <p>
    Once you've set your scope, plan your architecture. What game features do you need to make your MUD happen? This is why you shouldn't start with building — spending a few weeks planning out will show you what your game requires, and what you may find out is that it needs you to do rooms, NPCs and items entirely differently from how you would have done them starting out. For example, on <span class="gametitle">Epitaph</span> we went with a design philosophy for handling items that was completely different from what we had inherited as an assumption from <span class="gametitle">Discworld</span>. Rather than everything being hand-coded, we created special replaceable tokens and a set of properties that were inherited from the materials items could be made from. We could have spent a month developing an interesting series of weapons at the start, only to find we needed to rewrite them all once we properly decided on how they were going to work.
  </p>
  <p>
    After all of that, start coding. It doesn't matter where you start — pick a starting point and begin there. Get yourself some form of source control and just go nuts. Don't be afraid to gut your code to make it do what you want. Don't be afraid to experiment. Just make sure that, if the experiment goes wrong, you can roll back to when everything was working.
  </p>
  <p>
    Schedule a good chunk of time for this, but leave off area development until the later stages. You want to ensure that when you do get to the point of building your first few pieces of content, you don't have to waste time converting old code across into your new, shiny architecture.
  </p>
  <h3>Closed Alpha</h3>
  <p>
    Once you've been developing for a bit, I'd highly recommend bringing some people in. Not too many, and don't be indiscriminate, but the value of early player feedback is huge. It's been my experience that a stripped down system that’s fun will only become more so when it's fleshed out. On the other hand, if a stripped down system <em>isn’t</em> fun, no amount of beefing it up is going to help. Fundamental systems are either fun at the core or they’re not — there may be exceptions to this as a general rule, and if you bend a system that isn't fun <em>enough</em> you might just snap it in place.
  </p>
  <p>
    The earlier you find out if the core of your game is fun, the better. If it's not, you haven't sunk so much time into it that you can't really adjust your course — it’s better to know that early. If it is fun, it's a huge rush to know that people are going to enjoy what you have for them.
  </p>
  <p>
    A closed alpha period can last maybe a couple of weeks, and you should pay very close attention to what people are saying and playing. If there are systems they're not touching, ask yourself why. Sometimes it's a lack of signposting. Sometimes it's a skewed cost to benefit ratio. Sometimes it's that your system just ain't fun. Be ruthless with your code. Bad game systems are like tumors: they need to be cut out. On <span class="gametitle">Epitaph</span> I went through six or so incarnations of a temperature system that gave your outfits game impact. As it turned out, though, no matter how hard I tried, I just could not make it a fun system. So we got rid of it entirely.
  </p>
  <p>
    A whole suite of game mechanics were cut out in <span class="gametitle">Epitaph’s</span> closed alpha, and the game emerged from the process much better for it. We've gone through a number of alpha and beta cycles, open and closed, and each one has been more significant in terms of what was removed than what was added. Don't get me wrong — you'll be drowning in great ideas, and the enthusiasm of your players for certain things will be a barometer for what's neat about what you're doing. You'll find, though, that if they're enjoying what you've got out there, they'll generate more feedback and ideas than you can reasonably work your way through. That means you get to cherry-pick the best feedback and fold it into your later development.
  </p>
  <p>
    However, this kind of thing has the potential to make your scope run away from you. As in the planning phase, don't let people pull you into a spiral of new developments. Incorporate what you can and put a pin in the rest. Close the alpha relatively early, too — it's not a serious testing phase, it's really just a “kicking the tires” phase. While players are hanging around, you won't get much done, so thank them for their efforts, give them some kind of recognition for their support (on <span class="gametitle">Epitaph</span> we do this via badges), then lock the doors and get your head down.
  </p>
  <h3>Recruitment</h3>
  <p>
    What you'll likely find is that some of your alpha players might be inspired by what you're doing. That recruitment that was so hard at the beginning becomes a lot easier when people can really get a feel for what you're trying to accomplish. When our closed alpha on <span class="gametitle">Epitaph</span> ended, I received a number of offers from players saying they'd like to try their hand at development. My philosophy on this was always permissive.
  </p>
  <p>
    The problem with recruitment is this: people don't really know what it is they're getting into. As part of the alpha, you'll be busy beavering around, adding in new things, fixing problems as they arise, and from the perspective of someone who hasn't coded before, what you do may as well be magic. It's very seductive, the idea that you can step up in a world like this and just shape reality on a whim. Unfortunately, we all know better — learning how to code is, for most people, a long and arduous task that involves developing skills that are in many ways completely alien.
  </p>
  <p>
    Various ways exist to bridge the gap between required and actual ability, but all they do is delay the inevitable realization — being a coder or builder, even for a game you love, is <em>hard work</em>. With many people, as soon as they realize that what they've done is take on a second job that they need to study to be able to do, they fade away. I'd say of the dozens of people I have promoted to creator, both on <span class="gametitle">Epitaph</span> and on <span class="gametitle">Discworld</span>, fewer than ten percent of them have gone on to really become valuable and productive. That's putting no blame on them — the fact is that you need to have a burning desire in you to <em>want</em> to develop a game day in and day out.
  </p>
  <p>
    After your closed alpha, you'll be getting a number of offers of help, if all has gone well, so consider how best to parlay that into actual assistance. Remember <a href="http://en.wikipedia.org/wiki/The_Mythical_Man-Month">the mythical man-month</a> — all adding a new developer will do, in the short term, is slow you down. You need to be sure that you don't assume that, <em>finally</em>, you have others who will take some of your work off your back. If you're lucky, you'll get a couple of good recruits who absolutely will, in the fullness of time. Most of your recruits, though, will be a net drain on your time as you invest what effort you can in bringing them up to speed — effort that never pays off as they drift away.
  </p>
  <p>
    Don’t recruit too early. Most MUD coding is non-formal and often done through the use of examples. If you, as a player, see something in a game you like, and you want to do something “kinda like it”, then working from an example is vital. Early on in the process, you won’t have examples — some creators will be able to deal with that. Most won’t, and you’ll save yourself a lot of hassle by simply waiting until you do have a sufficiently large library of game code for people to use as a learning resource.
  </p>
  <h3>The Roadmap</h3>
  <p>
    Let's assume, though, that you've managed to con a few people into lending their support for free. It's time to really knuckle down and consider the shape of the first version of your game. What you need is a roadmap of some kind. Basically, this is an itemized list of coming versions of the game, what's going to be in them and who's responsible for making them happen. A roadmap can be as simple as a <a href="https://en.wikipedia.org/wiki/MoSCoW_Method">MoSCoW</a> analysis of your big ideas list or as formal as you like. On <span class="gametitle">Epitaph</span> we went somewhere in-between: formal patch numbers, with each patch on the wiki being given a list of our intended feature set. That list grows and shrinks day to day. As we headed into the last couple of months before release, the list swelled hugely as we did some internal playtesting and discovered many, many flaws. From that point on we whittled it down farther and farther until it had one entry: “fix remaining bugs”. I’m proud to say that we went to release with zero open bug reports that hadn’t been, in some way, considered.
  </p>
  <p>
    We did something relatively unusual early on <span class="gametitle">Epitaph</span> — we split the game into two versions. A lot of MUDs have “clones” where creators can log on and perform risky patches without breaking the game, but as far as I know it's still the norm that changes in most MUDs are made to the live game. The clone, if it exists at all, is a sandbox for creator experimentation. We went the other way — we set up a dedicated development MUD, which is where all the active development is done. Most creators have no access on the live MUD to make any code changes. We then put in place a patch architecture that allowed us to cleanly update content from the dev MUD (known as “Black Ops”) to the live MUD. We then set up a <a href="http://epitaphonline.co.uk/patches.c">formal patch notes system</a>, one that listed each of the changes under a discrete version number, so everyone who wanted to could track the evolution of the game.  In a development environment like this, you can't just patch on a whim because you fixed a few bugs, but you gain unparalleled development discipline. It gave us complete developer freedom whilst ensuring that the game experience remained consistent and reliable for everyone. It also ensured that we could version the software properly, which made the roadmap easier to handle.
  </p>
  <p>
    We put in a (few, moving) dates for when the initial 1.0 release would happen. As time passed, the roadmap for that version grew, and occasionally shrank, as features were added, evaluated and removed. We were pretty transparent about it — the roadmap was hidden from players on our developer wiki, but our patch notes were always available. A side benefit of doing this was that it generated a good deal of excitement amongst our pre-release players — there was a healthy buzz about the coming patch, what was involved, and when it was going to happen.
  </p>
  <p>
    The roadmap is the thing that will ensure you keep a steady course on development without veering too far away from your goal, which is an eventual release of a full-featured game.
  </p>
  <h3>Planning for Release</h3>
  <p>
    No game is ever going to be perfect. No game is ever going to be bug-free. No game the size and complexity of your average MUD is even going to come close to either of those goals. The fact is, you are never going to <em>feel</em> ready for release and you'll always find more reasons not to do it. Until you release, though — until people are playing your game and doing so with “live ammo” — you don't have a game, you've got a technical project. Your game <em>needs</em> to be released. Birds gotta fly, fish gotta swim, and games gotta be played.
  </p>
  <p>
    A game that releases is infinitely better than a game that doesn't. It doesn't matter how bad the released game is or how (theoretically) great the unreleased one is. This was absolutely the hardest lesson for me to learn, and the most difficult thing to accept. I would happily have continued tinkering with <span class="gametitle">Epitaph</span> for several more years without actually releasing, but at that point it would be obvious that I never would. So, I picked a date that came in the middle of my annual leave from work, which meant I had many full time days I could sink into polishing up what we had. Unfortunately, the first few weeks were unproductive because of a horrible heat wave, so I extended the date to the weekend before my leave ended. Those two weeks were a blur of activity — fixing bugs, testing, adding things, etc. As the release date loomed, I still felt as if we weren't ready, but I needed to keep reminding myself “we're never going to be <em>ready enough</em> for you”. I knew we had bugs aplenty, but then, every game does. We'd reached the point where any time we invested in polishing the game was having rapidly diminishing impact. You need players to find the bugs and report the bugs, and the only way they can do that is if they can play.
  </p>
  <h3>Release Day</h3>
  <p>
    My first piece of advice here is: leave plenty of time for opening day to make sure everything is ready. My second: make backups at least daily, and make an extra backup immediately before release. I had a very direct object lesson in the importance of backups: when I attempted to release <span class="gametitle">Epitaph</span>, I accidentally copied the old, public 0.5 version on top of the current 1.0 version that was to be released.
  </p>
  <p>
    Without backups, that would have been game over. The MUD would not have recovered. As it was, I hadn’t made a backup immediately before release, so some of the work since the last nightly backup was lost (what wasn’t recoverable from people’s open editors and local files), and the release was delayed. With a fresh backup, nothing would have been lost at all.
  </p>
  <p>
    Next piece of advice: don't list or advertise too early. Rely on the people who have been following your progress since the start, since they're the ones most likely to be forgiving, which they'll have to be. Develop a hard skin, and make sure everyone knows what a new release of a MUD involves. No matter how much work you put in, no matter how exhaustively you tested, your first contact with real players will be an enlightening experience.
  </p>
  <p>
    You'll be starting, broadly speaking, from a base of complete newbies. The first members of your nascent player base are just crawling around like children without adults, without anyone to give them “player useful” advice. Moreover, they don't know how everything works, so they're stress testing the mechanics in all kinds of interesting ways. They’re trying to eat living rats or slash people with trousers. They’re doing all the things that you wouldn’t have expected because you know the right way to play your game.
  </p>
  <p>
    Code tends to drift over time, too; something that was working the last time you tested may not be working by the time you go live, and you're just a handful of people with only your spare time to invest. Most projects can't do rigorous regression testing of the entire game every time something is changed.
  </p>
  <p>
    Expect to spend the next couple of days aggressively firefighting. We got <span class="gametitle">Epitaph</span> to the point where there were no longer any critical errors, so we could focus on the less tangible things like adjusting balance and penalties and so on. At that point, though, all we'd done is plug the bigger leaks — the boat was still taking on water. This is a situation you need to be ready for, which means actively fixing things. People will see that you're working hard and they'll be accommodating, but that's predicated on the assumption they want to see you succeed, so you want your long-term supporters around for this stage. Leave yourself enough time to deal with new people and reported issues. If it's just you, give yourself at least a week. If you have other reliable people with you, you can cut that down a bit.
  </p>
  <p>
    When you feel that the game is going to be solid enough for people who don't care if you succeed, throw out some ads for it. If you advertise on MUD-related sites, don't expect a huge influx of people. Most of the regular people frequenting these sites are developers or players who already have a favorite location. You'll get a few interested souls coming along, but many will be set in their ways already; if they're used to a DikuMUD, for example, even the syntax might be a turnoff. There are more ways to lose players in the first few minutes than there are to keep them.
  </p>
  <p>
    I would suggest a three-tier advertising strategy if you can manage it: your existing base of interested parties first, then mudders generally, and lastly some group of people outside the normal mudding world. <span class="gametitle">Epitaph</span> is a post-apocalyptic game, so we could reasonably advertise to zombie game and book fans. These are going to be the people who are hardest to satisfy, so don't try to recruit them right away. You'll get exactly one chance to make a first impression on them. Make sure that what they see is polished enough to draw them in.
  </p>
  <h3>Conclusion and Lessons Learned</h3>
  <p>
    I have worked on no project larger and more intense than <span class="gametitle">Epitaph</span> in my life. I have poured more of my blood and tears into this game than I did into my PhD. The evenings, weekends, and holidays devoted to this MUD have made it the equivalent of a second full-time job. I haven't had a proper vacation since I started; I've had weekends here, evenings there, a few snatched hours every other evening to do something with Mrs. Drakkos.
  </p>
  <p>
    The third biggest lesson I learned during this process was: you have to be able to do that. The reason why most MUDs never open isn't because of a lack of talent, or a lack of vision. It's because of a lack of <em>hunger</em>. I have <em>hungered</em> to release <span class="gametitle">Epitaph</span>, and it is that, more than anything else, that has ensured I made the time to make it happen. It's not enough to shrug one day and say “Meh, I'll make a MUD”.
  </p>
  <p>
    The second biggest lesson I learned is: when release day comes, you'll be hit with a realization. Your job isn't over. It's really only just begun. What release day provides is a promise of wonders to come for those who are willing to give your game a try. Players are voracious, though, and they consume content at incredible rates. Give it a few weeks — a month if you're lucky — and you'll find your newly addicted players are hungry for your next offering. The MUD demands you feed it with time, effort and enthusiasm. Leave yourself some energy to do that before you eventually collapse.
  </p>
  <p>
    The biggest lesson of all was this: for all the hard work it was, it was also an absolute joy. This is related to the hunger element above. We all have our addictions. Some are hooked on drugs, others on adrenaline. I'm hooked on coding, and the most fulfilling coding you can do is for a project where there are no limitations beyond your own capabilities. It's intoxicating to code for the unfettered love of it, and that intoxication grows when you see people enjoying what you have done.
  </p>
  <h3>Further Reading</h3>
  <p>
    I wrote extensively on my experiences when developing <span class="gametitle">Epitaph</span>, and you can find them on the <a class="website" href="http://epitaph.imaginary-realities.com/wp/?cat=1">Epitaph Textual Intercourse blog</a>. When I say “extensively”, I'm not kidding. There are around a quarter of a million words of content in there at the current count. I doubt the development of any MUD has ever been so exhaustively documented.
  </p>
  <p>
    For those interested in seeing how <span class="gametitle">Epitaph</span> code looks, you might want to have a peek inside the <a class="website" href="http://www.imaginary-realities.com/epitaph.pdf">Epitaph Survival Guide</a>, which is our internal development textbook.
  </p>
  <p>
    If you want to come check <span class="gametitle">Epitaph</span> out, and I absolutely think you should, then come see us. The grim darkness of the zombie apocalypse is waiting for you to make it just a little bit brighter.
  </p>

		<p class="staffbio authorbio">
    Drakkos (Michael Heron) is the lead developer of <a class="gametitle" href="http://epitaphonline.co.uk/">Epitaph</a>.
</p>
<h3>References</h3>
		</div>
	</div>

	<div class="mainsection">
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'imaginaryrealities';
    var disqus_identifier = 'website/volume-05/issue-01/hunger-game-learned-break-ship-bottle/index.html';
    var disqus_url = 'http://journal.imaginary-realities.com/volume-05/issue-01/hunger-game-learned-break-ship-bottle/index.html';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	</div>


	</div>
	</main>
	<footer>
	    <div class="footer">
			<div class="section">
			  <a href="../../../copyright.html"><img class="mini" alt="Copyright symbol icon" src="../../../images/copyright.svg"/><span>Copyright</span></a>
			  <a href="../../../contact.html"><img class="mini" alt="Email envelope icon" src="../../../images/email.svg"/><span>Contact Us</span></a>
			</div>
			<div class="section">
			<a href="http://journal.imaginary-realities.com/feed_rss2.xml"><img class="mini" alt="Standard RSS icon" src="../../../images/rss.svg"/><span>RSS</span></a>
			<a href="https://twitter.com/irjrnl"><img class="mini" src="../../../images/twitter.svg"/><span>Twitter</span></a>
			<a href="http://www.reddit.com/r/imaginaryrealities/"><img class="mini" alt="Reddit icon" src="../../../images/reddit.svg"/><span>Reddit</span></a>
			</div>
		</div>
	</footer>




    <div class="floating-box">
    <table class="floating-box-table floating-box-br">
      <tr>
        <td class="page-back floating-box-button unlinked-button">&#x00AB; issue</td>
        <td class="page-back floating-box-button "><a href='../introducing-new-players-redesigning-mud-school/index.html'>&#x00AB; article</a></td>
        <td class="page-forward floating-box-button "><a href='../help-save-old-mudding-resources/index.html'>article &#x00BB;</a></td>
        <td class="page-forward floating-box-button "><a href='../../../volume-06/issue-01/index.html'>issue &#x00BB;</a></td>
      </tr>
    </table>
    </div>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-264158-19', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>