<!doctype html>
<html lang="en">
<head>
	<!-- This page is generated from templates -->
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="alternate" type="application/rss+xml" href="http://journal.imaginary-realities.com/feed_rss2.xml" title="Imaginary Realities release feed"/>
	<link href="../../../css/reset.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style-desktop.css" rel="stylesheet" media="screen and (min-device-width:768px) and (min-width:768px), print, projection"/>

	<link href="../../../css/style-article.css" rel="stylesheet" media="screen, handheld, print, projection"/>

	<script src="../../../js/modernizr.js"></script>
  	
	<title>Imaginary Realities - 
	A well built zone is a work of art
</title>
</head>
<body>
    <header>
	<div class="header">
		<div class="header-name"><a href="../../../index.html">IMAGINARY REALITIES</a></div>
		<div class="header-name"><a href="../index.html">Volume 5, Issue 1</a></div>
	</div>
	<div class="headermenu">

		<span class="headermenu-link"><a href="../index.html">Table of Contents</a></span><span class="headermenu-link"><a href="../../../contribute.html">Contribute An Article</a></span>

	</div>
	</header>
	<main>
	<div class="main">

	<div class="mainsection">
		<div class="mainheader"><a name="article-title">
	A well built zone is a work of art
</a></div>
		<div class="mainbody">

<p class="byline">
by <span class="author-nick">Molly O’Hara</span>, 8th October 2013
</p>


  <h3>Not everyone is cut out to be a Builder</h3>
  <p>
    Anyone can slap on a badge that says “Builder” and learn the mechanics of <a href="http://en.wikipedia.org/wiki/Online_creation">OLC</a>. Very few have the stamina to actually finish their project; I think the general rate is about 5 percent. Even fewer manage to produce something really good, not just another rehash of some hack'n'slash zone that they played in some other MUD.
  </p>
  <p>
    Once it comes down to actually building the zone, you need to be organized and have lots of stamina to see the project through. Above all, though, a good Builder needs creativity, imagination, and a way with words. Some of what follows will be specific to MUDs similar to my home MUD, <a class="gametitle" href="http://4dimensions.org/">4Dimensions</a>, but these core qualities are universal.
  </p>
  <h3>Mapping the zone</h3>
  <p>
    The first step in building a zone is usually to make a map of it. The map provides two things: an overview of all the rooms included, and a check that their linking is logical. This is very important; every zone should be mappable on graph paper unless there’s an established reason that’s not the case. Even then, you need to put some consideration into how you link the rooms; you cannot just make a jumble of the exits. If you type ”<span class="code">look west</span>” in a room and see yourself a few rooms away, that is a dead give-away of bad mapping.
  </p>
  <h3>The zone needs a good story</h3>
  <p>
    The next step is to flesh out your original idea for the zone into a full background story, with a plot. You can write this down like you would a novel, then use it as a reference, with bits and pieces used to set up the right atmosphere in a room, in the description of a character or object, or as the theme for one or several built-in quests.
  </p>
  <p>
    Like a novel, a good zone needs to have a theme — a reason for existing — and everything in the zone needs to be consistent with that theme. This means <em>everything</em> in the zone — the time period, the terrain, the vegetation, the wildlife, the architecture, the inhabitants, the clothes, the weapons. No laser guns or cowboys in a medieval fantasy setting, or sudden swings between steamy jungle and icy tundra, and if the MUD has a general theme, everything needs to be consistent with that too.
  </p>
  <h3>Interacting with the zone</h3>
  <p>
    Creating a zone for a text MUD is a bit like writing a novel, but a MUD is interactive. In a way it is more complex, since you cannot just write the text down in one sequence, like you do with a novel; it needs to be chopped up in small pieces, suitable to fit into a room. In addition to writing the room descriptions, you need to consider the objects that can be picked up or interacted with in different ways, and the characters that move in and interact with the environment. On top of that, you have the scripts, which really bring life to the zone: characters that talk and answer questions, messages that bring atmosphere to the environment, obstacles and puzzles for the player to overcome and solve.
  </p>
  <p>
    All of this has to work together in every room to create an image of reality — or of a convincing fantasy setting. However, in a MUD there is also the scrolling text that appears continuously: when someone enters or leaves the room, performs an action or uses the global channels, and above all, the potentially overwhelming flow of messages when a fight is going on. The scrolling text makes the MUD come to life, but also adds to the spam on the player's screen, which has to be considered when you decide on the amount of information a room can contain.
  </p>
  <h3>The ideal length of the room description depends on the environment</h3>
  <p>
    Another thing that you need to consider is how things look when you move around. There is a big difference between a “travel zone” — an environment that you basically move through to get somewhere else — and a “goal zone”, where you are expected to remain for some time. For a travel zone to look good when you walk around in it, the room descriptions should be of even length and fairly short — two to four lines are ideal. Once you have reached a destination, be it a castle, a tavern or just a hidden cave behind some bushes, the room description can be longer and more detailed, since you expect the player to pay a bit more attention in an environment like that.
  </p>
  <p>
    Even in goal zones, there still is a limit to how long the room description should be. Spam is something you should always avoid in a zone. To me, seven lines is the maximum; any detail needed beyond that should be left to descriptions of the room’s contents. There are some builders — especially those who have attended classes in creative writing — who tend to go overboard with long, flowery descriptions. In doing so, they shoot themselves in the foot, since a MUD is not a bedside novel but an interactive game, and in less roleplay-oriented games, the majority of the players are really just out to kill monsters and pick up whatever good equipment they can find to make it easier to kill those monsters. So if the descriptions are too long, they will just engage brief mode and never see the lovely prose that you put so much work into.
  </p>
  <h3>Adding depth and detail</h3>
  <p>
    You do also want to cater to those that treat your zone like an interactive novel, actually reading everything they come across. This elite group of players is really why we put all that extra work into a zone. So now you utilize all the descriptive capabilities provided in your OLC. Ideally, there should be a description for every noun in the main room description. You can also put detailed descriptions on all <em>directions</em>, not just the directions you can walk in. In some MUDs, like the one I work for, there are also descriptions for <em>listen, smell, taste</em> and <em>feel</em>, and different room descriptions for daytime, nighttime, different seasons, and various weather conditions.
  </p>
  <p>
    Naturally, all characters and objects should have description too, preferably something interesting that relates to the theme of the zone or some quest that the player is expected to solve. This is where your background story starts to become useful. You can describe a character in ways that give hints of its capabilities and nature, not just what it looks like. You can make love letters to hide in drawers or caches, and even books that can be read by looking at the index, and then at chapter 1, chapter 2, and so on. Not everyone might bother to read those books at first, but if the information that you get that way is vital for solving a quest, they will soon start doing it.
  </p>
  <h3>Adding challenges</h3>
  <p>
    All this is still elementary. For a zone to be excellent, there also needs to be some kind of challenge, something more than just a really tough boss monster to kill. This is where <em>scripts</em> and <em>quests</em> come in. This is also where you can really use your background story — the logical thing to do is turn the plot into the basis for a main quest, usually carried out in several steps. The zone can also contain a number of mini-quests, where players can collect experience, gold, quest points, tokens, or currency by doing various tasks. These smaller tasks can be parts of the main quest or totally independent, but all of them should be in-theme, and also repeatable, to give the players an extra motivation to return to the zone — but for a lesser reward each time, since a quest is only really hard the first time you do it.
  </p>
  <p>
    There are basically three main ways to finish a character-driven quest in a MUD:
  </p>
  <ol>
   <li>give the character a specific item</li>
   <li>kill some other character and bring proof of it</li>
   <li>bring another character to it, perhaps a missing child or spouse</li>
  </ol>
  <p>
    Nearly every quest you encounter in any MUD is a variation on these three. Whether a quest is good or bad depends on how it is designed. Here, we get back to the theme and background story of the zone. How interesting, entertaining, and challenging a quest becomes depends equally on the quality of the plot and on how well the scripts are set up.
  </p>
  <h3>Don't spam the players</h3>
  <p>
    You can do almost anything conceivable with scripts — the only real limit is your imagination. This is really a subject for another article, because writing good and cheat-proof quest scripts is an art in itself. Before we leave the subject of scripts, though, let me add a warning about a trap that too many new builders fall into — SPAM.
  </p>
  <p>
    Scripts are often used to create a certain atmosphere in a zone — a sudden rustle in the leaves behind your back, a character who welcomes you into their house, a stray dog starting to follow you, that sort of thing. These are good additions to a zone as long as the scripts don't go off too often, in which case it soon becomes irritating instead. So, if you use a random trigger to set off the script, make sure that the chance of the script triggering isn't set too high. Also, be wary of “greet” or “entry” scripts that trigger every time someone enters the room. Remember that players often run back and forth several times when exploring a zone, and that non-player characters can trigger a script too, unless you take precautions against it.
  </p>
  <h3>Making it harder for the players</h3>
  <p>
    There are several tricks that you can use to make a zone harder beyond just raising the number and/or level of the monsters. You can, for instance, make a character or object that the player needs to find rather hard to get to. This can be achieved in many ways.
  </p>
  <p>
    The most obvious way is using a maze (which I'm not overly fond of, myself). Most MUD code also support hidden exits, but seasoned players quickly learn to type <span class="code">search</span> a couple of times in each room, and then the secret door isn't a secret any more.
  </p>
  <p>
    Portals (objects that transport you based on a specific command) are better, because they can be prevented from appearing in the room’s contents, so the player will have to read the descriptions in the room thoroughly to find out if there is a hidden portal in the room and how to enter it. For example, let’s say looking west provides the following description:
  </p>
  <p class="quotetext">
    “An embroidered tapestry hangs on the west wall.”
  </p>
  <p>
   <span class="code">look tapestry</span> gives the result:
  </p>
  <p class="quotetext">
    “The tapestry depicts a maiden sitting in a field of flowers. It hangs slightly askew.”
  </p>
  <p>
   <span class="code">look behind tapestry</span> finally gives:
  </p>
  <p class="quotetext">
    “Behind the tapestry is a secret passageway.”
  </p>
  <p>
    The command <span class="code">enter secret passageway</span> then moves the PC to a new location.
  </p>
  <p>
    The same method can be used for hidden containers. After looking around a bit in the room, you might discover that there is a king-size bed against the east wall. The command <span class="code">look bed</span> may just give a description of the sheets and pillows, but <span class="code">look under bed</span>
    could reveal a chamber pot, which in turn might contain something more interesting than the usual.
  </p>
  <p>
    This can be varied in many ways. The trick is to not choose too-obvious names for the portal or container, since experienced players learn to routinely type things like <span class="code">enter hole</span> or <span class="code">get all from hole</span> each time they find a new room, if they notice a pattern in the aliases used. However, you also need to be fair and provide a clue to the identity of the hidden objects so that players don't need to resort to <a href="http://en.wikipedia.org/wiki/Syntax_guessing">noun or verb guessing</a>. The rule in my MUD is that if there is an object in a room that the players need to interact with and that doesn’t appear in the room’s contents, the name needed to address it it must also be mentioned in one of the room descriptions.
  </p>
  <p>
    You can also use gadgets like a sturdy safe with a combination lock. Figuring out the combination could be as simple as finding a note with some digits on it or as involved as talking to many NPCs in order to find out the name of the owner's pet dog.
  </p>
  <p>
    If you want to be even more subtle, you can use things like a mithril inscription on a stone wall that only becomes visible at midnight or when hit by the light of the full moon, or fruits and berries that only ripen in autumn. (This means that the code of your MUD must support things like day and night descriptions, seasons and moon phases. If it doesn't, you need to pester your coders until they add it.)
  </p>
  <h3>Balanced rewards</h3>
  <p>
    Finally, we get to quest rewards.
  </p>
  <p>
    All Builders want their zones to be popular, and the easiest way to achieve that is to add a piece of overpowered equipment and hope that it slips through quality control.
  </p>
  <p>
    Don't fall for this easy trick. Overpowered equipment might make your zone popular, at least until every player has collected the item, but it doesn't make it a good zone. On the contrary, it makes it a bad one, since it upsets the balance of the entire MUD.
  </p>
  <p>
    The reward can't be too small, either. If the players need to go through a long and elaborate quest to finally get to the reward, they will feel cheated if they don't find it worth the effort.
  </p>
  <p>
    There are two main rules for making rewards:
  </p>
  <ol>
   <li>If a reward is a weapon or piece of equipment, its quality should be in proportion to how hard it is to get.</li>
   <li>If the reward is gold, experience, trade points, or quest tokens, it needs to be substantially higher the first time you do a quest and then become lower for each repetition.</li>
  </ol>
  <h3>Summary</h3>
  <p>
    So to sum this up: what constitutes a really good zone? Below is my own list:
  </p>
  <ul>
   <li>Consistency with theme</li>
   <li>Good background story</li>
   <li>Logical linking (unless justified otherwise)</li>
   <li>Appropriate length of descriptions</li>
   <li>Detail and depth</li>
   <li>Challenges and puzzles</li>
   <li>Nifty gadgets and tricks</li>
   <li>Balanced rewards</li>
   <li>Correct spelling, grammar, and punctuation</li>
   <li>No spammy scripts</li>
   <li>No buggy scripts</li>
  </ul>
  <p>
    As you can see, I've put three rather important points at the bottom of my list. Why?
  </p>
  <p>
    Because nothing can replace a writer's creativity and imagination, and the way things are designed to interact in each room, but a good Head Builder can run the files through a spellchecker and fix up the bad scripts. Sometimes it’s a good idea to handle the last three items that way, as a way of avoiding having the Head Builder be too nitpicky in feedback to other Builders.
  </p>

		<p class="staffbio authorbio">
    Molly O'Hara is the Zone Coordinator and Administrator of Game Architecture and Design for <a class="gametitle" href="http://4dimensions.org/">4Dimensions</a>.
</p>
<h3>References</h3>
		</div>
	</div>

	<div class="mainsection">
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'imaginaryrealities';
    var disqus_identifier = 'website/volume-05/issue-01/well-built-zone-work-art/index.html';
    var disqus_url = 'http://journal.imaginary-realities.com/volume-05/issue-01/well-built-zone-work-art/index.html';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	</div>


	</div>
	</main>
	<footer>
	    <div class="footer">
			<div class="section">
			  <a href="../../../copyright.html"><img class="mini" alt="Copyright symbol icon" src="../../../images/copyright.svg"/><span>Copyright</span></a>
			  <a href="../../../contact.html"><img class="mini" alt="Email envelope icon" src="../../../images/email.svg"/><span>Contact Us</span></a>
			</div>
			<div class="section">
			<a href="http://journal.imaginary-realities.com/feed_rss2.xml"><img class="mini" alt="Standard RSS icon" src="../../../images/rss.svg"/><span>RSS</span></a>
			<a href="https://twitter.com/irjrnl"><img class="mini" src="../../../images/twitter.svg"/><span>Twitter</span></a>
			<a href="http://www.reddit.com/r/imaginaryrealities/"><img class="mini" alt="Reddit icon" src="../../../images/reddit.svg"/><span>Reddit</span></a>
			</div>
		</div>
	</footer>




    <div class="floating-box">
    <table class="floating-box-table floating-box-br">
      <tr>
        <td class="page-back floating-box-button unlinked-button">&#x00AB; issue</td>
        <td class="page-back floating-box-button "><a href='../modern-interface-modern-mud/index.html'>&#x00AB; article</a></td>
        <td class="page-forward floating-box-button "><a href='../journey-through-paradice/index.html'>article &#x00BB;</a></td>
        <td class="page-forward floating-box-button "><a href='../../../volume-06/issue-01/index.html'>issue &#x00BB;</a></td>
      </tr>
    </table>
    </div>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-264158-19', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>