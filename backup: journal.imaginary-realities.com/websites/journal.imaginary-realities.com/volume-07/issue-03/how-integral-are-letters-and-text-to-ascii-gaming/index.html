<!doctype html>
<html lang="en">
<head>
	<!-- This page is generated from templates -->
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="alternate" type="application/rss+xml" href="http://journal.imaginary-realities.com/feed_rss2.xml" title="Imaginary Realities release feed"/>
	<link href="../../../css/reset.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style-desktop.css" rel="stylesheet" media="screen and (min-device-width:768px) and (min-width:768px), print, projection"/>

	<link href="../../../css/style-article.css" rel="stylesheet" media="screen, handheld, print, projection"/>

	<script src="../../../js/modernizr.js"></script>
  	
	<title>Imaginary Realities - 
	How integral are letters and text to ASCII gaming?
</title>
</head>
<body>
    <header>
	<div class="header">
		<div class="header-name"><a href="../../../index.html">IMAGINARY REALITIES</a></div>
		<div class="header-name"><a href="../index.html">Volume 7, Issue 3</a></div>
	</div>
	<div class="headermenu">

		<span class="headermenu-link"><a href="../index.html">Table of Contents</a></span><span class="headermenu-link"><a href="../../../contribute.html">Contribute An Article</a></span>

	</div>
	</header>
	<main>
	<div class="main">

	<div class="mainsection">
		<div class="mainheader"><a name="article-title">
	How integral are letters and text to ASCII gaming?
</a></div>
		<div class="mainbody">

<p class="byline">
by Mark R. Johnson, 25th May 2015
</p>


    <p>
Text, and the letters which constitute text, are essential to roguelikes. Although their <span class="italicd">roguelikelike</span>, <span class="italicd">roguelite</span> and <span class="italicd">procedural death labyrinth</span> brethren have, in recent years, espoused a non-ASCII philosophy and gone the (for some people, blasphemous) route of developing non-textual graphics in a roguelike, all the classics were originally created using an ASCII-only console display, and many players argue in favour of continuing to play them in this mode, even after the advent of a range of tilesets. This piece is an exploration of some of the functions textual characters and text more generally serve in the genre — beyond the obvious function of aesthetic nostalgia and visually defining a “classic roguelike” — as well as an examination of some of the more cryptic and obscure uses of that same text, and the way I’m attempting in my own game (<a class="gametitle" href="http://www.ultimaratioregum.co.uk">Ultima Ratio Regum</a>) to move the roguelike preference for text into interesting new directions.
    </p>

    <h3>Glyphs as representation</h3>
    <p>
One of the most important function text and letters serve in roguelikes is to identify the differences and the class relationships between monsters. In ASCII-mode classic roguelikes we find that all monsters are typically depicted with the letters a-z or A-Z, sometimes the numbers 0-9, and sometimes a number of other additional characters (most often <span class="code">:</span>, <span class="code">;</span> and <span class="code">&amp;</span>). As I’ve talked about in a forthcoming paper for <span class="italicd">Games and Culture</span>, the nature of roguelike semiotics and the translation of roguelike semantic codes is a surprisingly complex process, but suffice to say that these letters — or “glyphs”, as we tend to call them — are used to display everything the player needs to make sense of in the in-game map. They are not, in this regard, used as letters, but rather as abstract signifiers of various monsters or items, which in almost all cases (the use of <span class="code">s</span> for snakes in many games being a rare exception) bear no visual similarity to that which they depict. Comparably, in Lisa Gotto’s (2014) exploration of the game <span class="gametitle">Type Rider</span>, she notes that in that game the original meaning of each letter is separated from the design of the letter: they function only as abstract shapes, stripped of any associated semantic meaning. Although roguelikes do attempt to link the letter to the class being represented — <span class="code">h</span> for humanoids, <span class="code">a</span> for ants, <span class="code">W</span> for wraiths and other wraith-like monsters, and so on — the precise connections in each game are challenging to divine for a new player. Choosing which letter to use to represent which class results in a range of different categorical ontologies (again, see my <span class="italicd">Games and Culture</span> piece) with few clues ever given to the player about which letter adheres to which logic until these have been learned by rote. Making sense of what glyph represents what monster is crucial — few if any visual cues are offered, but this remains a central and essential function of letters in roguelikes.
    </p>

	<p>
	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image00.jpg" style="width: 568.00px; height: 320.00px;" title="" />
	  </div>
    </p>

    <h3>Text as agential information</h3>
    <p>
The second use of ASCII, where we see “text” come to the fore instead of “glyphs”, is in the message log. This is a list of recent happenings in the game (generally since the player’s last turn) where information which cannot be readily displayed in a glyph-based interface is listed for the player’s perusal. This includes information about which monsters moved last turn, who they attacked, who was killed, any magical spells launched, weapons thrown, weapons picked up or dropped, new enemies who strayed into the player’s sight, messages from the player’s deity, and many other pieces of non-visual information not easily depicted for the player using an interface which can only display a foreground colour and a character (or, in some rare and particularly daring cases, those and a variable background colour). Message logs are designed to be extremely clear, but in some cases a player will encounter messages whose meaning is not immediately apparent. Examples might be the “level feeling” messages a player gets upon entering a new floor in <span class="gametitle">Angband</span>, designed to give the player some idea of what awaits them on that floor, or some of the “You feel” messages in <span class="gametitle">NetHack</span>. These range from the fairly clear — “You feel wide awake!” means that to you have acquired sleep resistance — to the less obvious — “You feel in control of yourself” for the acquisition of “teleport control” — to the rather difficult to decipher — “You feel more claustrophobic than before”, which indicates that a damaged wall in a shop has been repaired whilst the player is standing within that same shop, <span class="italicd">but</span> didn’t see it actually happen. In these slightly stranger messages, we begin to see another purpose to which text has been put: as a form of puzzle, or something approaching one.
    </p>

	<p>
	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image02.png" style="width: 601.73px; height: 361.71px;" title="" />
	  </div>
    </p>

    <h3>Cryptic Roguelike Messages</h3>
    <p>
Taking this idea of cryptic messages even further, some roguelikes have used text to offer clues, hints or puzzles to the player, without generally explicitly stating them as such, and relying on the player’s intuition and understanding of how roguelike designers think to get them through safely. <span class="gametitle">ADOM</span>, for example, contains a massive range of possible statues the player can encounter. These all have a unique description, and the majority also contain a written inscription which, along with the description of the statue’s (often unusual) appearance, forms the second part of an implicit riddle. Take, for example, this statue description from <span class="gametitle">ADOM</span>:
    </p>
    <p class="quotetext">
“This is the statue of a tall human magician, clothed in a long tunic. A decided expression is on his face, and a beautifully-sculpted flame raises from his extended hand.”
    </p>
    <p>
The statue bears the inscription “Ubun, the fire elementalist. Igne nature renovatur integra”, which translates from Latin approximately to “nature is renewed by fire”. Similarly, consider another statue, described thus:
    </p>
    <p class="quotetext">
“It resembles a huge egg, hewn from polished granite.”</span>
    </p>
    <p>
Its inscription reads “Only a fool would try to break me”. If one prays at the base of the first statue, a fireball is generated upon the player; if one kicks the second statue, the player receives a small amount of damage whilst the statue remains intact. In both of these cases, the descriptions of the statues are obviously intended to offer a minor puzzle to the player, and encourage them to think about what might happen when that statue is interacted with.
    </p>
    <p>
A trickier statue to decipher is this:
    </p>
    <p class="quotetext">
“Delicately carved from granite and smoothly polished, a young, lithe woman lies prostrate on an ornate settee. She is clothed in thin, revealing attire, with one hand resting wearily against her forehead, and the other reaching languidly outwards. Her posture suggests great personal despair.”</span>
    </p>
    <p>
Its inscription is “...and 'lo that wench, upon this bench, her fate was sealed in stone”. The information about the statue and the attendant inscription suggest that nothing good is likely to happen upon interacting with this statue, but no obvious clues are given as to what fate might befall the player; in any roguelike there are quite a few reasons why a character might be in such a sorry state. As it turns out, if you step on the statue, your player character becomes deafened by a howling screech, meaning that you can no longer hear the words of others, nor interact with them yourself (since you “don’t know sign language”, as the game somewhat sardonically notes). There is, however, a clue here — and a benefit to being deaf — but a far more obscure clue than the other two. On a late dungeon level the player will encounter a unique monster, the Banshee, who will instantly kill an unprepared player with an unearthly wail. Upon entering the floor containing this monster, a message about “sorrow and death” (as the <span class="gametitle">ADOM</span> wiki page puts it) will be printed; similarly, the Banshee’s mythology emphasizes (or at least implies) the death of a husband, and recurring themes of despair and tragedy. It does not seem like such a leap to suggest that this statue depicts the Banshee, before her death and transformation into a ghost but after the death of her husband. With that said: this is a tenuous connection (even though stepping onto it aids one against the Banshee!) and requires the player to have encountered, or at least heard about, the Banshee to make sense of it, followed by a logical leap.
    </p>
	<p>
	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image01.png" style="width: 601.73px; height: 352.68px;" title="" />
	  </div>
    </p>
    <p>
We can therefore see that whilst the first two examples are pretty clear — assaulting a statue of a fire elementalist seems unlikely to result in the player being doused with cool, refreshing water (unless that kind of reversal had been set up elsewhere!), whilst an egg statue seems to be almost begging to be broken — the Banshee statue is far more cryptic, and I would be surprised if many players were able to decipher it before interacting with it (although perhaps there is an <span class="gametitle">ADOM</span> “way of thinking” which can be cultivated by experienced players?). Nevertheless, it sets the groundwork for an interesting use of text — albeit fixed text where a player who knows what Statue X does on one playthrough will also know what Statue X does on the next playthrough — and it is this use I’d like to explore a little more. We’ve seen that glyphs are used to display monsters and the interrelationships between them, while full textual sentences are used to both further flesh out an ASCII world, and in some cases offer minor puzzles and riddles for the player to ponder over. All well and good — but what else could be done with text? At this point we drift into my own work, and future intentions, for my game <span class="gametitle">Ultima Ratio Regum</span>.
    </p>

    <h3>Text as integral gameplay</h3>
    <p>
Anyone who has been following <span class="gametitle">Ultima Ratio Regum</span> a little while will remember a short lived version, 0.3 I believe (0.4 also?), where the player could explore ziggurats within which lay procedurally-generated puzzles. These were puzzles where the player was offered a range of blocks each bearing a design of some sort — a sun, a moon, a spider, a tree (with four possible seasons), a piranha, a jaguar, various other animals and physical objects — and the game offered a number of slots into which these blocks could be fitted, along with cryptic messages like “the spinner lurks to the east of the new growth” (which means that the block depicting a spider should be placed to the right of a block depicting a tree in blossom) or “the lifegiver rises north of the silent stalker” (sun above a jaguar). The game was able to generate an immense number of such puzzles (ranging from simple 2x2 grids of blocks needed for the solution up to 4x4 grids) and simultaneously work out the absolute minimum number of clues required for the puzzle to be solvable, therefore making each puzzle as challenging as it could be while ensuring it could be completed. Back then, the goals of the game were somewhat nebulous and unformed, and these ziggurat puzzles have since been removed (though they may return?), and are now best understood as a kind of proof of concept for the question “can I get the game to generate its own <span class="italicd">textual</span> riddles and puzzles, albeit in a very small and contained environment?” In this case, of course, as soon as the player understands how the puzzle is solved once, it can be solved that way every time, even if the clues, locations and “layers” of clues may increase, and therefore in some sense the true “puzzle” is only there once, until the player understands how they are expected to make sense of the clue, and subsequently manoeuvre the component blocks. It was an interesting experiment, and received very positive feedback, but ultimately only pointed the way towards scaling this type of system up to the entire game, which is what I now aim to do.
    </p>
	<p>
	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image04.png" style="width: 461.00px; height: 445.00px;" title="" />
	  </div>
    </p>
    <p>
The intention is to have the game generate a range of different forms of text. For example, as it stands now, the world contains a vast history, but this is crucially a history put into <span class="italicd">narrative</span> rather than simply presented as “fact” a la <span class="gametitle">Dwarf Fortress</span> — different nations will produce competing histories, and histories must be uncovered, and will continue to resonate in the world the player explores. Settlements and individuals are replete with <span class="italicd">informative</span> nicknames that speak to their history, ideologies, or achievements, without simply resorting to piecing together two random words; procedural books will generate telling of historical events, or narratives which give the player hints (so a novel about a certain city’s jail might give the player some subtle information about that jail, which may prove useful); procedural poetry will speak to important figures from the world’s past and present; the conversations one has with NPCs should yield useful data about their nations, cultures, religions, social norms and biases, and the like, without ever being too obvious about it. I should note I in no way mean the comment earlier in this paragraph to disparage <span class="gametitle">Dwarf Fortress</span>, glorious masterwork that it is, but rather to emphasise my interest here in semantic procedural generation where <span class="italicd">the player can divine deeper meaning</span> from everything they read, rather than a procedurally-generated world where meaning and ideology are not the focus of the game’s design (since obviously <span class="gametitle">DF</span> has a thousand other design goals which <span class="gametitle">URR</span> does not). Equally, by making this all procedurally-generated, the player should never be able to learn that Inscription X is guaranteed to refer to Secret Y — this should be unique each time (though the player can of course learn the meta-rules which govern this type of interaction, which is an important part of roguelike mastery). A core part of this is the development of what I am loosely calling a “semantic converter” — still in development (but coming along nicely) at time of writing, this is a section of code which needs to be able to take any piece of <span class="italicd">data</span>, and transform it into any possible type of <span class="italicd">clue</span>. What I mean by this is the game must be able to input a date, a time, a person’s nickname, the design on a vase, the colour of brick in a particular civilization, the name of a painting, a historical battle, or anything else, and then <span class="italicd">convert</span> it, regardless of what it is, into a piece of poetry, a novel, something an NPC might say to you, a painting, a symbol or a sign, a holy book, a shop sign, or anything else which hints the player towards the underlying meaning and origin of what they read.
    </p>
    <p>
This, suffice to say, is no easy task.
    </p>
	<p>
	  <div class="imgcenter">
	   <img class="center" alt="" src="images/image03.png" style="width: 601.00px; height: 394.00px;" title="" />
	  </div>
    </p>
    <p>
However, despite how fantastic this all sounds (at least, I think it does!), one obvious issue arises: roguelikes are (normally) famed for their gameplay and the attendant <span class="italicd">lack</span> of narrative, storytelling, thematic elements, etc. Narrative and storytelling generally means text, and have roguelike players not perhaps come to expect games which are comparatively devoid of text which is not immediately essential? By this I mean that all roguelike players of course acknowledge the importance of a detailed and comprehensive message log (for there is nothing more painful than the feeling of an “unfair” demise in a permadeath game) but is anything beyond that seen as something more suited to other genres?
    </p>
    <p>
This is a complex question, and beyond the scope of this piece to unpick, but I’d like to offer some brief concluding thoughts which suggest the answer is that this kind of dense textual content I’m proposing for <span class="gametitle">Ultima Ratio Regum</span> will certainly be alien and novel, but not disliked or against the rationale of prior classic roguelike design. Firstly, we can look to <span class="gametitle">Tales of Maj’Eyal</span>, currently one of the most well-played “classic” roguelikes out there. <span class="gametitle">TOME</span> contains a large volume of plot information. Most of the in-game quests are story arcs in their own right or tell their own stories, and there are many tens of thousands of words of lore information for the dedicated player to read. However, the obvious objection arises: this is optional content! One does not have to read it, let alone understand it, if the player endeavour to success in the continent of Maj’Eyal. I propose making this deciphering of, and interaction with, a game’s narrative elements an essential part of the game’s success: it should be impossible to complete unless the player understands deeply the procedurally-generated world they find themselves within. The gameplay <span class="italicd">is</span> understanding the lore.
    </p>
    <p>
Secondly, <span class="gametitle">Ultima Ratio Regum</span> is designed as a game where combat will be extremely rare. Various roguelike players and designers have noted that a player basically massacres the entire population of the dungeon on a given playthrough, and an average playthrough in most classic roguelikes will result in a death count in the thousands. By contrast, I imagine no more than a dozen combat situations in the average successful playthrough (and obviously far fewer in an unsuccessful playthrough), where combat is something rare and significant, rather than something which is primarily a grind before then offering a rare challenging fight (so, in effect, I have removed all popcorn enemies). What therefore takes the place of combat? <span class="italicd">Decipherment!</span> Investigation, exploration, examination, deduction. Is that really so distinct from what roguelikes ask of us already, even if we don’t tend to conceptualize this as a core gameplay mechanic? What I’m proposing here is undoubtedly a new kind of “riddle”/“investigation”-based gameplay, but is it that different to other roguelikes and their use of text? We are used to scrutinising every message log and decoding every glyph, and I’d suggest that — although taking this point to its logical extreme — what I’m building in <span class="gametitle">Ultima Ratio Regum</span> is a continuation of this venerable tradition, not a shift towards the quagmire of story and exposition that we seem to think the quotidian roguelike player is so terribly fearful of.
    </p>

		<p class="staffbio authorbio">
	Mark R. Johnson is the creator of <a class="gametitle" href="http://www.ultimaratioregum.co.uk">Ultima Ratio Regum</a>.
</p>

	<h3>References</h3>
	<ol start="1">
	<li>Gotto, L. (2014). “Types and Bytes. Ludic Seriality and Digital Typography.” In <span class="publicationtitle">Eludamos. Journal for Computer Game Culture</span>, 8:1, 115-128. Retrieved from <a class="publicationtitle" href="http://www.eludamos.org/index.php/eludamos/article/viewArticle/vol8no1-8/8-1-8-html">http://www.eludamos.org/index.php/eludamos/article/viewArticle/vol8no1-8/8-1-8-html"</a></li>
	</ol>

		</div>
	</div>

	<div class="mainsection">
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'imaginaryrealities';
    var disqus_identifier = 'website/volume-07/issue-03/how-integral-are-letters-and-text-to-ascii-gaming/index.html';
    var disqus_url = 'http://journal.imaginary-realities.com/volume-07/issue-03/how-integral-are-letters-and-text-to-ascii-gaming/index.html';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	</div>


	</div>
	</main>
	<footer>
	    <div class="footer">
			<div class="section">
			  <a href="../../../copyright.html"><img class="mini" alt="Copyright symbol icon" src="../../../images/copyright.svg"/><span>Copyright</span></a>
			  <a href="../../../contact.html"><img class="mini" alt="Email envelope icon" src="../../../images/email.svg"/><span>Contact Us</span></a>
			</div>
			<div class="section">
			<a href="http://journal.imaginary-realities.com/feed_rss2.xml"><img class="mini" alt="Standard RSS icon" src="../../../images/rss.svg"/><span>RSS</span></a>
			<a href="https://twitter.com/irjrnl"><img class="mini" src="../../../images/twitter.svg"/><span>Twitter</span></a>
			<a href="http://www.reddit.com/r/imaginaryrealities/"><img class="mini" alt="Reddit icon" src="../../../images/reddit.svg"/><span>Reddit</span></a>
			</div>
		</div>
	</footer>




    <div class="floating-box">
    <table class="floating-box-table floating-box-br">
      <tr>
        <td class="page-back floating-box-button "><a href='../../../volume-07/issue-02/index.html'>&#x00AB; issue</a></td>
        <td class="page-back floating-box-button "><a href='../dispelling-the-gloom/index.html'>&#x00AB; article</a></td>
        <td class="page-forward floating-box-button "><a href='../legend-and-the-lore/index.html'>article &#x00BB;</a></td>
        <td class="page-forward floating-box-button unlinked-button">issue &#x00BB;</td>
      </tr>
    </table>
    </div>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-264158-19', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>