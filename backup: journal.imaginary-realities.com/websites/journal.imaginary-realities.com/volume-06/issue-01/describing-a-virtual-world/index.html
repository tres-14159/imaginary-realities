<!doctype html>
<html lang="en">
<head>
	<!-- This page is generated from templates -->
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="alternate" type="application/rss+xml" href="http://journal.imaginary-realities.com/feed_rss2.xml" title="Imaginary Realities release feed"/>
	<link href="../../../css/reset.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style.css" rel="stylesheet" media="screen, handheld, print, projection"/>
	<link href="../../../css/style-desktop.css" rel="stylesheet" media="screen and (min-device-width:768px) and (min-width:768px), print, projection"/>

	<link href="../../../css/style-article.css" rel="stylesheet" media="screen, handheld, print, projection"/>

	<script src="../../../js/modernizr.js"></script>
  	
	<title>Imaginary Realities - 
	Describing a Virtual World
</title>
</head>
<body>
    <header>
	<div class="header">
		<div class="header-name"><a href="../../../index.html">IMAGINARY REALITIES</a></div>
		<div class="header-name"><a href="../index.html">Volume 6, Issue 1</a></div>
	</div>
	<div class="headermenu">

		<span class="headermenu-link"><a href="../index.html">Table of Contents</a></span><span class="headermenu-link"><a href="../../../contribute.html">Contribute An Article</a></span>

	</div>
	</header>
	<main>
	<div class="main">

	<div class="mainsection">
		<div class="mainheader"><a name="article-title">
	Describing a Virtual World
</a></div>
		<div class="mainbody">

<p class="byline">
by Richard <span class="author-nick">KaVir</span> Woolcock, 2nd February 2014
</p>


  <p class="c0"><span>The subject of dynamic descriptions has been thrown around for many years, yet surprisingly few
  MUDs make use of them for describing their worlds, perhaps in part because of the many misconceptions about how they
  work</span> <span class="c6">&#8212;</span><span>&#160;in particular, the mistaken assumption that
  &quot;dynamic&quot; is synonymous with &quot;computer-generated&quot;, and that &quot;computer-generated&quot; means
  the computer writes the entire description from scratch.</span></p>

  <p class="c0"><span>In this article I describe the divide between static and dynamic, and between handwritten and
  computer-generated, and then show some examples of how I use computer-generated dynamic descriptions in my own
  project.</span></p>

  <h3>Handwritten static descriptions</h3>

  <p class="c0"><span>This is the approach used by most MUDs for writing room and object descriptions, help files, and
  so on. Someone writes the description, and that&#39;s exactly how everyone else sees it.</span></p>

  <p class="c0"><span>Obviously this imposes some pretty severe limitations on the writer, as they need to produce
  descriptions that are appropriate to all viewers at all times, regardless of whether it&#39;s summer or winter, day
  or night, or the viewer is a giant or a halfling, sitting or standing, etc.</span></p>

  <p class="c0"><span>Some people try to work around these limitations by imposing strict rules and guidelines, such as
  not using the word &quot;you&quot; and requiring that all room descriptions be written in the third person, despite
  the rest of the MUD being written from the more-immersive second person perspective. This writing style is hardly
  ideal for an interactive game.</span></p>

  <p class="c0"><span>It&#39;s worth noting that, while many people defend the use of third-person in room
  descriptions, you don&#39;t see them applying the same rule elsewhere; nobody wants to see &quot;a weapon is
  drawn&quot; rather than &quot;you draw your sword&quot;. Yet the use of static room descriptions is so prevalent and
  ingrained that many people take them for granted, and even defend their limitations without really considering the
  alternatives.</span></p>


  <h3>Computer-generated static descriptions</h3>


  <p class="c0"><span>This approach is rare, because if you&#39;re going to the effort of generating descriptions
  anyway, then you might as well make them dynamic. However, it&#39;s occasionally used by MUDs with large wilderness
  areas, or to generate default &quot;filler&quot; descriptions that can later be replaced or modified by
  hand.</span></p>


  <p class="c0"><span>A common misconception is that the computer writes the descriptions from scratch, and that they
  will therefore be poorly written. In fact, the descriptions are usually assembled from randomly selected handwritten
  sentences or paragraphs, with further randomisation applied to certain words, in order to produce a large number of
  unique descriptions with much less effort than writing the same number of descriptions by hand.</span></p>

  <p class="c0 c7"></p>

  <p class="c0"><span>However the end result is still static and will be the same for all viewers. Such descriptions
  can also become repetitive unless a sufficient amount of pre-written text is provided, so the staff will still need
  to invest a fair amount of work; a handwritten world grows in</span> <span class="c5">size</span><span>&#160;the more
  effort you put into it, while a computer-generated world grows in</span> <span class=
  "c5">detail</span><span>&#160;the more effort you put into it.</span></p>


  <h3>Handwritten dynamic descriptions</h3>


  <p class="c0"><span>These produce the best results, but also require the most work. Much like handwritten static
  descriptions, a real person has to write them, but the difference is that dynamic descriptions can include variables
  and conditional checks</span> <span class="c6">&#8212;</span><span>&#160;for example, describing the snow crunching
  beneath the viewers&#8217; boots as they walk, but only when it&#39;s actually winter, and when the viewer is
  currently walking, while wearing boots.</span></p>


  <p class="c0"><span>Some MUDs already offer separate static descriptions for day and night, but dynamic descriptions
  take the concept much further, allowing you to include whatever factors you consider appropriate for each particular
  description.</span></p>


  <p class="c0"><span>It should be stressed that dynamic descriptions are just a tool; you don&#39;t have to use them
  for every description. For example, you might choose to use them only for specific details that couldn&#39;t
  otherwise be included in a static description, such as sunlight reflecting off a statue (which should only occur
  during sunset), or translating the meaning of the words carved above an ancient goblin temple (which should only
  occur if the viewer can read ancient goblin script).</span></p>


  <p class="c0"><span>This approach is actually very common in most MUDs, but only for event messages. For example,
  DikuMUDs often write messages in the format</span> <span class="code">$n draws $s $o</span><span>, which might be
  displayed as &quot;KaVir draws his sword&quot;. Technically, that&#39;s a very simple hand-written dynamic
  description, as are the customisable prompts offered by most modern MUDs. One could even argue that the use of
  customisable colour codes in room descriptions is also a form of dynamic description, although that&#39;s only really
  true from a technical perspective.</span></p>


  <h3>Computer-generated dynamic descriptions</h3>


  <p class="c0"><span>Much like the computer-generated static descriptions, these are generally assembled from pieces
  of handwritten text. The difference is that these descriptions are usually generated on the fly and/or use some sort
  of markup language, so that they&#39;re customised for each viewer and each viewing.</span></p>


  <p class="c0"><span>It can sometimes be difficult to draw a line between a handwritten and computer-generated dynamic
  description, but the former is usually written for a specific thing (one particular creature, object, etc.), while
  the latter is applied to a whole category of things. For example, a handwritten dynamic description might apply to
  one specific room, while a computer-generated dynamic description might apply to millions of rooms (the actual output
  would obviously vary from room to room, but all the descriptions would be produced using the same system).</span></p>


  <h3>Describing locations</h3>


  <p class="c0"><span>My MUD doesn&#39;t use rooms, so I lacked the obvious anchor for descriptions, and decided that
  the easiest approach would be to generate dynamic descriptions based on what the viewer could currently see. After
  trying various different techniques, I finally settled on generating a series of sections of text which could be
  combined to form the final description:</span></p>


  <ul>
  <li><span class="c3">Section 1:</span> Viewer&#39;s position (standing, walking, flying, etc.), season, and terrain.</li>
  <li><span class="c3">Section 2:</span> Time and terrain.</li>
  <li><span class="c3">Section 3:</span> Weather and terrain.</li>
  <li><span class="c3">Section 4:</span> Optional hand-written area description (season and day/night specific).</li>
  <li><span class="c3">Section 5:</span> Nearby landmarks at your level of elevation or higher (if available).</li>
  </ul>

  <p class="c0"><span>Each section usually consists of a single sentence, but can be longer</span> <span class=
  "c6">&#8212;</span><span>&#160;or may be combined with another section, if both are particularly short. For example,
  in the following description you can see that sections 2 and 3 have been merged together in the second
  sentence:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image01.png" width="601"/>
  </div>

  <p class="c0"><span>On the other hand, here&#8217;s a different example in which sections 1, 2, 3, and 4 each have
  their own sentence, but where there are no nearby landmarks visible for generating section 5:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image12.png" width="601"/>
  </div>

  <p class="c0"><span>As you may have noticed, the descriptions also mention that I&#8217;m walking and wearing boots.
  That&#8217;s the sort of thing you can&#8217;t assume in a static description, but because the descriptions are
  dynamic, they&#8217;re automatically adjusted based on the viewer. This solution allows you to use any and all
  information that the MUD might have available, because you no longer need to make assumptions about the viewer. You
  could even reference emotions, if the MUD tracks them (such as phobias chosen during character creation).</span></p>


  <p class="c0"><span>For example I might not be walking, and I might not have boots. Come to think of it, I might not
  even be</span> <span class="c5">human</span><span>...</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image03.png" width="602"/>
  </div>

  <h3>Describing creatures</h3>


  <p class="c0"><span>Most MUDs offer a command for players to write their own static descriptions. I wanted to avoid
  that approach for a couple of reasons. Firstly, I don&#39;t run a roleplaying MUD, and most of my players would have
  left their descriptions blank, or filled them with quotes or ASCII art. Secondly, I wanted character descriptions to
  be accurate, consistent, and factor in worn equipment and current form (as shape changing is an important part of the
  game), which meant including &#8220;naked&#8221; descriptions for characters who weren&#8217;t wearing
  clothing.</span></p>


  <p class="c0"><span>As most characters are humanoid, that&#8217;s what I focused most of my attention on. Inspired by
  the approach I&#8217;d used for locations, I decided to split the description into six sections:</span></p>

  <ul>
  <li><span class="c3">Section 1:</span> Appearance (or face armour) and hair (or head armour).</li>
  <li><span class="c3">Section 2:</span> Body, arms and forearms equipment (or naked upper body).</li>
  <li><span class="c3">Section 3:</span> Waist, groin, legs, shins and feet equipment (or naked lower body).</li>
  <li><span class="c3">Section 4:</span> Height and physical grace.</li>
  <li><span class="c3">Section 5:</span> Held weapons and hand-worn armour.</li>
  <li><span class="c3">Section 6:</span> Sheathed weapons.</li>
  </ul>

  <p class="c0"><span>The first section describes your head (hair) and face (attractiveness), but only if those
  features are visible. You might wear a mask which covers just your face, a hat which covers just your head, or a
  helmet which covers both locations. You might wear a cloak with a hood, or an open helm with a visor that can be
  lowered. You might even wear a headband that goes around your head without actually concealing your hair.</span></p>


  <p class="c0"><span>The second and third sections cover the upper body (including arms and hands) and lower body
  (including legs and feet) respectively, and can be combined in certain situations</span> <span class=
  "c6">&#8212;</span><span>&#160;for example, when wearing a dress or robe, or when naked. They also need to factor in
  different layers, because although you can&#8217;t see a vest under a shirt, you</span> <span class=
  "c5">can</span><span>&#160;see a shirt under a breastplate, because the breastplate covers only the torso while the
  shirt also covers the arms. There are also certain pieces of armour (such as bracers and greaves) which don&#8217;t
  cover their location entirely, so if you wore a breastplate and bracers over a chainmail shirt, then all three would
  be visible.</span></p>


  <p class="c0"><span>I ended up defining torso and groin as the &#8220;naked&#8221; locations, so that if your torso
  is uncovered you&#8217;re described as being naked from the waist up, while an uncovered groin describes you as being
  naked from the waist down, and if both are uncovered then you&#8217;re simply described as being naked. Other
  locations are noted as exceptions, for example you might be naked &#8220;except for a pair of boots&#8221;. The naked
  descriptions then factor in tattoos, physical build, and so on, in order to generate something
  appropriate.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image17.png" width="602"/>
  </div>

  <p class="c0"><span>Another nice side effect of generating the descriptions is that I can automatically change the
  perspective when you look at yourself.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image02.png" width="602"/>
  </div>

  <p class="c0"><span>The actual features (attractiveness, skin, eyes and hair) are configured by the player using a
  simple &#8220;appearance&#8221; command. The command offers different options for different forms, although some of
  them share the same data; if you have black hair in human form, and shape change into a wolf, you&#8217;ll also have
  black fur.</span></p>

  <p class="c0"><span>However, other forms required their own solutions. It was particularly challenging trying to
  describe a vaguely humanoid-shaped mist that can</span> <span class="c5">also</span><span>&#160;wear
  equipment.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image16.png" width="602"/>
  </div>

  <p class="c0"><span>Not all of the forms are so clearly defined. In particular, demons are able to fully customise
  their appearance by collecting and applying different &#8220;warps&#8221; to their body:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image10.png" width="602"/>
  </div>

  <p class="c0"><span>This solution has also proven to be a very quick and effective way of generating monster
  descriptions, as I can simply assign them the appropriate traits and the MUD does the rest. I&#8217;ve used it for
  describing a wide range of creatures, and in one case I even used it to populate randomly-generated dungeons with
  randomly-generated monsters. Because the warps actually change your body shape (activating specialised movement and
  combat abilities), the descriptions are actual descriptive text, not just cosmetic flavour.</span></p>

  <p class="c0"><span>For the short descriptions you see when interacting with a creature, I generally use handwritten
  or computer-generated static descriptions, but in a former MUD project (in which I also used an introduction system)
  I used computer-generated dynamic descriptions for PCs which factored in both appearance and equipment being carried,
  so you might see a &#8220;tall, dark-haired man wielding a pair of knives&#8221;.</span></p>

  <h3>Describing objects</h3>

  <p class="c0"><span>I don&#8217;t actually generate descriptive flavour text when someone looks at an object, as the
  goal is simply to provide an overview of the item type and its attributes; however, I do use a number of other
  dynamic elements.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image22.png" width="601"/>
  </div>

  <p class="c0"><span>The short descriptions you see when interacting with an object are computer-generated dynamic
  descriptions, allowing the descriptions to be updated on the fly to indicate whether a sword is notched,
  blood-stained, rusty, and so on. This doesn&#8217;t just look cool, it also provides useful information about the
  item: a rusty weapon is easier to damage, wet armour provides better protection against heat damage (but leaves the
  wearer more vulnerable to electrical damage), and so on. This is the sort of attention to detail that can help bring
  the game alive, and make it feel more immersive, as you wade knee-deep into a pond and see that all your uncovered
  equipment below knee level has become wet.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image06.png" width="601"/>
  </div>

  <p class="c0"><span>This is also a good example of the way in which objects interact with creatures and the world.
  Consider what happens when a wet character assumes an elemental form of fire, and then equips themselves with two
  items with different melting points:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image05.png" width="601"/>
  </div>

  <h3>Describing help files</h3>


  <p class="c0"><span>Hand-written dynamic descriptions are a highly effective way to present help files, allowing you
  to include hints and examples tailored specifically to the individual and highlight information that might be of
  interest to a particular viewer.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image00.png" width="601"/>
  </div>

  <p class="c0"><span>Now consider how the same technique could be applied to the help file for a quest:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image21.png" width="601"/>
  </div>

  <p class="c0"><span>The status has been colour-coded to indicate that I&#8217;ve already completed the quest, while
  the flavour text itself is tailored towards a vampire</span> <span class="c6">&#8212;</span><span>&#160;because the
  viewer</span> <span class="c5">is</span><span>&#160;a vampire. If that same help file were viewed by a dragon, for
  example, then they&#8217;d see the following:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image20.png" width="601"/>
  </div>

  <p class="c0"><span>The technique is particularly effective when applied to help files for abilities, as it allows
  the viewer to see at a glance exactly what requirements they need to fulfill.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image09.png" width="601"/>
  </div>

  <p class="c0"><span>Although the informative text within help files usually needs to be handwritten, there are other
  areas where you can make use of computer-generated output. For example, if the player requests a help file that
  doesn&#8217;t exist, you could use a soundex algorithm to search for a similar-sounding keyword.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image13.png" width="602"/>
  </div>

  <p class="c0"><span>The soundex algorithm isn&#8217;t always very helpful on its own, as it matches by sound rather
  than meaning, but I&#8217;ve found it works well in combination with a text search</span> <span class=
  "c6">&#8212;</span><span>&#160;even if the requested keyword lacks its own help file, it might still be mentioned
  within other help files.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image07.png" width="602"/>
  </div>

  <p class="c0"><span>Another useful technique, particularly for the benefit of commonly asked questions, is to select
  help files based on combinations of keyword and trigger them from a public channel.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image11.png" width="602"/>
  </div>

  <p class="c0"><span>The above help file is linked to the sequence &#8220;how |i|you| |open|pick|
  |lock|chest|chests|&#8221;, which means it&#8217;s a match if the text contains &#8220;how&#8221;, either
  &#8220;I&#8221; or &#8220;you&#8221;, either &#8220;open&#8221; or &#8220;pick&#8221;, and either &#8220;lock&#8221;,
  &#8220;chest&#8221;, or &#8220;chests&#8221;. The tokens must be in sequence, and any other tokens are ignored.
  Here&#8217;s another, this time for &#8220;how |i|you| |lower|raise|open|close| |visor|eyes|&#8221;.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image04.png" width="601"/>
  </div>

  <h3>Describing combat</h3>

  <p class="c0"><span>Most MUDs already have dynamic combat messages, but don&#39;t take the concept very far. Usually
  they include little more than the name and gender of the attacker and target, and a message based on how much damage
  they inflicted. The descriptions can be made much more interesting if they also factor in body shapes:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image19.png" width="602"/>
  </div>

  <p class="c0"><span>That looks like a fairly regular combat message, but what happens if you shoot lightning at
  something that doesn&#8217;t</span> <span class="c5">have</span><span>&#160;a head?</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image08.png" width="602"/>
  </div>

  <p class="c0"><span>A guardhouse doesn&#8217;t have a head, but it certainly has a wall</span> <span class=
  "c6">&#8212;</span><span>&#160;and in this case it also has a custom death message based on the damage type
  (exploding from the lightning). The same concept can be extended to any number of opponents, producing appropriate
  cosmetics for whatever body shape they might have:</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image14.png" width="601"/>
  </div>

  <p class="c0"><span>Even the random treasure drops have appropriate descriptions (in this case a chest floating from
  the wreckage), to explain how a pirate ship &#8220;creature&#8221; is able to drop loot.</span></p>

  <h3>The &#8220;display&#8221; command</h3>

  <p class="c0"><span>Having implemented a system for generating dynamic descriptions, it was a simple matter to add a
  &#8220;display&#8221; command that allows players to interface with the system directly.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image18.png" width="602"/>
  </div>

  <p class="c0"><span>Combined with a simple alias system, this allows players to create their own shortcuts for
  displaying any information they might need. It&#39;s particularly useful for blind players, enabling them to access
  specific information without having to suffer the spam of a prompt or filtering the necessary data from a score
  sheet.</span></p>

  <h3>Active descriptions</h3>

  <p class="c0"><span>One last technique I&#8217;d like to touch on is that of active descriptions: instead of the
  descriptions always appearing in bursts, this approach generates a continuous flow of information as the character
  moves around. Conceptually it isn&#8217;t really any different from the way most MUDs already handle weather and
  day/night messages, it&#8217;s just been extended to cover a wider range of messages.</span></p>

  <div class="imgcenter">
   <img class="center" src="images/image15.png" width="602"/>
  </div>

  <p class="c0"><span>Sending too much data can overload the player and feel like spam, but if handled with care this
  technique can provide the user with a form of ongoing narrative of their progress. It&#8217;s particularly effective
  for blind players, as it&#8217;s faster to parse than big blocks of text.</span></p>

  <h3>Summary</h3>

  <p class="c0"><span>When using handwritten static descriptions, you can typically expect a well-written world to
  require about 30-60 minutes work per room, taking into account extra descriptions, object and monster descriptions,
  proofreading, etc. <span class="publicationtitle">The Mud Connector</span> defines a medium-sized world as 3000-8000 rooms, so even if you treated building
  as a 9-to-5 job, it would probably take you a couple of years of solid work to create a decent medium-sized world. If
  you&#8217;re using handwritten dynamic descriptions, then you can expect it to require even longer.</span></p>

  <p class="c0"><span>Even if you&#39;re able to invest that much time, you may decide there are other things you&#39;d
  rather spend it on. Using computer-generated descriptions is one approach to freeing up that time. Any strategy for
  managing a project&#8217;s resources has its pros and cons, and there&#8217;s no universal &#8220;right&#8221; or
  &#8220;wrong&#8221; way of doing it. However, time is a finite resource, so if you want to create a playable MUD,
  then at some point you&#8217;ll have to make compromises.</span></p>

		<p class="staffbio authorbio">
	KaVir (Richard Woolcock) is the owner of &#160;<a href="http://www.godwars2.org/%E2%80%8E">God Wars II</a>.
</p>
<h3>References</h3>
		</div>
	</div>

	<div class="mainsection">
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'imaginaryrealities';
    var disqus_identifier = 'website/volume-06/issue-01/describing-a-virtual-world/index.html';
    var disqus_url = 'http://journal.imaginary-realities.com/volume-06/issue-01/describing-a-virtual-world/index.html';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	</div>


	</div>
	</main>
	<footer>
	    <div class="footer">
			<div class="section">
			  <a href="../../../copyright.html"><img class="mini" alt="Copyright symbol icon" src="../../../images/copyright.svg"/><span>Copyright</span></a>
			  <a href="../../../contact.html"><img class="mini" alt="Email envelope icon" src="../../../images/email.svg"/><span>Contact Us</span></a>
			</div>
			<div class="section">
			<a href="http://journal.imaginary-realities.com/feed_rss2.xml"><img class="mini" alt="Standard RSS icon" src="../../../images/rss.svg"/><span>RSS</span></a>
			<a href="https://twitter.com/irjrnl"><img class="mini" src="../../../images/twitter.svg"/><span>Twitter</span></a>
			<a href="http://www.reddit.com/r/imaginaryrealities/"><img class="mini" alt="Reddit icon" src="../../../images/reddit.svg"/><span>Reddit</span></a>
			</div>
		</div>
	</footer>




    <div class="floating-box">
    <table class="floating-box-table floating-box-br">
      <tr>
        <td class="page-back floating-box-button "><a href='../../../volume-05/issue-01/index.html'>&#x00AB; issue</a></td>
        <td class="page-back floating-box-button "><a href='../building-a-mech-in-evennia/index.html'>&#x00AB; article</a></td>
        <td class="page-forward floating-box-button "><a href='../dynamic-room-descriptions/index.html'>article &#x00BB;</a></td>
        <td class="page-forward floating-box-button "><a href='../../../volume-07/issue-01/index.html'>issue &#x00BB;</a></td>
      </tr>
    </table>
    </div>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-264158-19', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>